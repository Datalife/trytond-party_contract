# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from sql.conditionals import Coalesce
from sql.operators import Equal as SQL_Equal
from trytond.model import ModelSQL, ModelView, fields, Workflow, Exclude
from trytond.pyson import Eval, If, Bool, Equal, Not
from trytond.modules.product import price_digits
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from decimal import Decimal
from trytond.config import config
from trytond.exceptions import UserError
from trytond.i18n import gettext


__all__ = ['PartyContract', 'PartyContractLine', 'PartyContractDiscount']

STATES = [
    ('draft', 'Draft'),
    ('confirmed', 'Confirmed'),
    ('finished', 'Finished'),
    ('cancelled', 'Cancelled')]

discount_digits = (16, config.getint('product', 'discount_decimal',
    default=4))


class PartyContract(Workflow, ModelSQL, ModelView):
    '''Party Contract'''
    __name__ = 'party.contract'

    type_ = fields.Selection([
        (None, '')], 'Type', states={
            'readonly': Eval('state') != 'draft'
        }, depends=['state'])
    party = fields.Many2One('party.party', 'Party', required=True,
        ondelete='CASCADE', states={
            'readonly': Eval('state') != 'draft'
        }, depends=['state'])
    start_date = fields.Date('Start date', required=True, states={
        'readonly': Eval('state') != 'draft'
        }, depends=['state'])
    end_date = fields.Date('End date',
        domain=['OR',
                ('end_date', '=', None),
                ('end_date', '>=', Eval('start_date'))],
        states={
            'readonly': Eval('state').in_(['cancelled', 'finished']),
            'required': Eval('state') == 'finished'
        }, depends=['state', 'start_date'])
    state = fields.Selection(STATES, 'State', required=True, readonly=True)
    lines = fields.One2Many('party.contract.line', 'contract', 'Lines',
        states={
            'readonly': Not(Equal(Eval('state'), 'draft'))},
            depends=['state'])
    description = fields.Char('Description',
        states={
            'readonly': Not(Eval('state').in_(['draft', 'confirmed']))},
        depends=['state'])
    other_party = fields.Many2One('party.party', "Other party",
        ondelete='CASCADE',
        domain=[('id', '!=', Eval('party'))],
        states={
            'readonly': Eval('state') != 'draft',
        },
        depends=['party', 'state'])

    @classmethod
    def __setup__(cls):
        super(PartyContract, cls).__setup__()
        cls._order.insert(0, ('start_date', 'ASC'))

        cls._transitions |= set((
            ('draft', 'cancelled'),
            ('draft', 'confirmed'),
            ('cancelled', 'draft'),
            ('confirmed', 'draft'),
            ('confirmed', 'cancelled'),
            ('confirmed', 'finished'),
            ('finished', 'confirmed')
            ))
        cls._buttons.update({
            'cancel': {
                'invisible': ~Eval('state').in_(['draft', 'confirmed']),
                'depends': ['state'],
            },
            'draft': {
                'invisible': ~Eval('state').in_(['cancelled', 'confirmed']),
                'icon': If(Eval('state') == 'cancelled', 'tryton-undo',
                    'tryton-back'),
                'depends': ['state'],
            },
            'confirm': {
                'invisible': ~Eval('state').in_(['draft', 'finished']),
                'icon': If(Eval('state') == 'draft', 'tryton-forward',
                        'tryton-back'),
                'depends': ['state'],
            },
            'finish': {
                'invisible': ~Eval('state').in_(['confirmed']),
                'icon': 'tryton-ok'
            }
            })

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        super().__register__(module_name)

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

    def get_rec_name(self, name):
        if self.description:
            return '%s [%s]' % (self.description, self.party.name)
        return '%s' % self.party.name

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('description', ) + tuple(clause[1:]),
            ('party.name',) + tuple(clause[1:]),
        ]

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, records):
        pass

    @classmethod
    def validate(cls, records):
        super(PartyContract, cls).validate(records)
        for record in records:
            record.check_overlaping_contracts()

    def check_overlaping_contracts(self):
        domain = [
            ('id', '!=', self.id),
            ('party', '=', self.party.id),
            ('type_', '=', self.type_),
            ['OR',
                ('end_date', '=', None),
                ('end_date', '>=', self.start_date)
            ],
            ('other_party', '=', self.other_party
                and self.other_party.id or None)
        ]
        if self.end_date:
            domain.append(('start_date', '<=', self.end_date))

        overlaping_contracts = self.search(domain)
        if overlaping_contracts:
            raise UserError(gettext(
                'party_contract.msg_party_contract_overlaping_contract',
                current_contract=self.rec_name,
                overlaped_contract=overlaping_contracts[0].rec_name))


class PartyContractLine(ModelSQL, ModelView):
    '''Party Contract Line'''
    __name__ = 'party.contract.line'

    contract = fields.Many2One('party.contract', 'Contract', required=True,
        readonly=True, select=True, ondelete='CASCADE')
    start_date = fields.Date('Start date', required=True,
        domain=[
            ('start_date', '>=',
                Eval('_parent_contract', {}).get('start_date'))],
        states={
            'readonly': Eval('contract_state') != 'draft'},
        depends=['contract_state'])
    template = fields.Many2One('product.template', 'Template',
        ondelete='CASCADE',
        states={
            'readonly': Eval('contract_state') != 'draft',
            'required': Not(Bool(Eval('product', False)))
            },
        depends=['contract_state', 'product'])
    product = fields.Many2One('product.product', 'Product',
        ondelete='CASCADE',
        domain=[
            If(Bool(Eval('template', False)),
                ('template', '=', Eval('template')), ())],
        states={
            'readonly': Eval('contract_state') != 'draft',
            'required': Not(Bool(Eval('template', False)))
            },
        depends=['contract_state', 'template'])
    uom = fields.Many2One('product.uom', 'UOM',
        states={
            'readonly': Eval('contract_state') != 'draft',
            'required': Not(Equal(Eval('unit_price', None), None))},
        depends=['contract_state', 'unit_price'])
    uom_digits = fields.Function(fields.Integer('UOM Digits'),
        'get_uom_digits')
    unit_price = fields.Numeric('Unit Price', digits=price_digits,
        states={
            'readonly': Eval('contract_state') != 'draft'},
        depends=['contract_state'])
    contract_state = fields.Function(
        fields.Selection(STATES, 'Contract State'),
        'get_contract_state')

    @classmethod
    def __setup__(cls):
        super(PartyContractLine, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('template_product_contract_date_exclude', Exclude(t,
                (t.contract, SQL_Equal),
                (t.start_date, SQL_Equal),
                (Coalesce(t.product, -1), SQL_Equal),
                (Coalesce(t.product, t.template), SQL_Equal)),
                'party_contract.msg_party_contract_line_'
                'template_product_contract_date_exclude'),
            ]

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        pool = Pool()
        Product = pool.get('product.product')
        product = Product.__table__()

        table = cls.__table_handler__(module_name)

        template_exist = table.column_exist('template')
        super(PartyContractLine, cls).__register__(module_name)
        if not template_exist:
            table.drop_constraint('product_contract_date_uniq')
            cursor.execute(
                *sql_table.join(product,
                    condition=(sql_table.product == product.id)
                ).select(
                    sql_table.product, product.template,
                    group_by=(sql_table.product, product.template)))
            for product_id, template_id in cursor.fetchall():
                cursor.execute(*sql_table.update(
                    columns=[sql_table.template],
                    values=[template_id],
                    where=sql_table.product == product_id))

    @classmethod
    def get_uom_digits(cls, records, name=None):
        return {r.id: r.uom.digits for r in records}

    def get_contract_state(self, name):
        if self.contract:
            return self.contract.state

    @fields.depends('contract', '_parent_contract.state',
        '_parent_contract.start_date')
    def on_change_contract(self):
        if self.contract:
            self.contract_state = self.contract.state
            self.start_date = self.contract.start_date


class PartyContractDiscount(metaclass=PoolMeta):
    __name__ = 'party.contract'
    discount = fields.Numeric('Discount', digits=discount_digits,
        domain=[If(Bool(Eval('discount')),
                [
                    ('discount', '>', 0),
                    ('discount', '<=', 1)],
                [])],
        states={
            'readonly': Not(Equal(Eval('state'), 'draft'))},
        depends=['state'])

    @classmethod
    def default_discount(cls):
        return Decimal(0)
