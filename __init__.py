# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import party


def register():
    Pool.register(
        party.PartyContract,
        party.PartyContractLine,
        module='party_contract', type_='model')

    Pool.register(
        party.PartyContractDiscount,
        module='party_contract', type_='model',
        depends=['account_invoice_discount'])
