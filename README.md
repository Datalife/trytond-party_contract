datalife_party_contract
=======================

The party_contract module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-party_contract/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-party_contract)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
